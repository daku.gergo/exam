<%@ page import="java.util.List" %>
<%@ page import="java.util.NoSuchElementException" %>
<html>
<body>
<h2>Hello World!</h2>

<%
    List<Integer> lista = (List<Integer>) request.getAttribute("Lista");
    if(!lista.isEmpty()){
        Integer min = lista.stream().mapToInt(m -> m).min().orElseThrow(NoSuchElementException::new);
        session.setAttribute("MIN", min);
    }
%>
</body>
</html>
