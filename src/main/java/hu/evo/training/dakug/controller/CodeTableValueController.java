package hu.evo.training.dakug.controller;

import hu.evo.training.dakug.ejb.CodeTableValueFacade;
import hu.evo.training.dakug.entity.CodeTableValueEntity;

import javax.ejb.EJB;
import javax.faces.bean.SessionScoped;
import javax.inject.Named;
import java.util.List;

@Named("codeTableValueController")
@SessionScoped
public class CodeTableValueController {

    @EJB
    private CodeTableValueFacade ejbFacade;
    private List<CodeTableValueEntity> items = null;
    private CodeTableValueEntity selected;

    public CodeTableValueController() {
    }

    public CodeTableValueFacade getEjbFacade() {
        return ejbFacade;
    }

    public void setEjbFacade(CodeTableValueFacade ejbFacade) {
        this.ejbFacade = ejbFacade;
    }

    public List<CodeTableValueEntity> getItems() {
        if (items == null) {
            items = getEjbFacade().findAll();
        }
        return items;
    }

    public void setItems(List<CodeTableValueEntity> items) {
        this.items = items;
    }

    public CodeTableValueEntity getSelected() {
        return selected;
    }

    public void setSelected(CodeTableValueEntity selected) {
        this.selected = selected;
    }
}
