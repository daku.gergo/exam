package hu.evo.training.dakug.entity;


import javax.persistence.*;
import java.io.Serializable;

@Table(name = "kodtabla_mezo")
@Entity
public class CodeTableFieldEntity implements Serializable {

    private static final long serialVersionUID = -668040797583317378L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "mezonev")
    private String fieldName;

    @Column(name = "adattipus")
    private String dataType;

    @Column(name = "leiras")
    private String description;

    public CodeTableFieldEntity() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getDataType() {
        return dataType;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "CodeTableFieldEntity{" +
                "id=" + id +
                ", fieldName='" + fieldName + '\'' +
                ", dataType='" + dataType + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
