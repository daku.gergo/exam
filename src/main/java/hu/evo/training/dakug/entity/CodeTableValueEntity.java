package hu.evo.training.dakug.entity;


import javax.persistence.*;
import java.io.Serializable;

@Table(name = "kodtabla_ertek")
@Entity
public class CodeTableValueEntity implements Serializable {

    private static final long serialVersionUID = -8715065269003017944L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "ertek")
    private String value;

    @Column(name = "sorrend")
    private Integer order;

    @JoinColumn(name = "km_id")
    @ManyToOne
    private CodeTableFieldEntity codeTableFieldEntity;

    public CodeTableValueEntity() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }

    public CodeTableFieldEntity getCodeTableFieldEntity() {
        return codeTableFieldEntity;
    }

    public void setCodeTableFieldEntity(CodeTableFieldEntity codeTableFieldEntity) {
        this.codeTableFieldEntity = codeTableFieldEntity;
    }

    @Override
    public String toString() {
        return "CodeTableValueEntity{" +
                "id=" + id +
                ", value='" + value + '\'' +
                ", order=" + order +
                '}';
    }
}
