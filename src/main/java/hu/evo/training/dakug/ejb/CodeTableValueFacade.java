package hu.evo.training.dakug.ejb;

import hu.evo.training.dakug.entity.CodeTableValueEntity;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Stateless
public class CodeTableValueFacade extends AbstractFacade {

    @PersistenceContext(unitName = "exam")
    private EntityManager em;

    protected EntityManager getEntityManager() {
        return em;
    }

    public CodeTableValueFacade() {
        super(CodeTableValueEntity.class);
    }

    public List<CodeTableValueEntity> findAll() {
        javax.persistence.criteria.CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(CodeTableValueEntity.class));
        return getEntityManager().createQuery(cq).getResultList();
    }
}
