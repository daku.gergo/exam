CREATE SCHEMA `codetable` ;

CREATE TABLE `codetable`.`kodtabla_mezo` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `mezonev` VARCHAR(45) NOT NULL,
  `adattipus` VARCHAR(45) NOT NULL,
  `leiras` VARCHAR(4000) NULL,
  PRIMARY KEY (`id`));
  
CREATE TABLE `codetable`.`kodtabla_ertek` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `ertek` VARCHAR(255) NOT NULL,
  `sorrend` INT NOT NULL,
  `km_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `id_idx` (`km_id` ASC) VISIBLE,
  CONSTRAINT `id`
    FOREIGN KEY (`km_id`)
    REFERENCES `codetable`.`kodtabla_mezo` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

INSERT INTO `codetable`.`kodtabla_mezo` (`id`, `mezonev`, `adattipus`) VALUES ('1', 'mezo1', 'String');
INSERT INTO `codetable`.`kodtabla_mezo` (`id`, `mezonev`, `adattipus`) VALUES ('2', 'mezo2', 'Integer');

INSERT INTO `codetable`.`kodtabla_ertek` (`id`, `ertek`, `sorrend`, `km_id`) VALUES ('1', 'Java', '2', '1');
INSERT INTO `codetable`.`kodtabla_ertek` (`id`, `ertek`, `sorrend`, `km_id`) VALUES ('2', 'Python', '1', '2');


  